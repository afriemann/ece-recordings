#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: TODO
   :platform: Unix
   :synopsis: TODO.

.. moduleauthor:: Aljosha Friemann aljosha.friemann@gmail.com

"""

import logging #, subprocess

#from sh import ffmpeg, ErrorReturnCode

from . import system

logger = logging.getLogger(__name__)

def record(vdevice, vcodec, adevice, acodec, output_path,
           preset = 'ultrafast', threads = 0, vformat='v4l2', aformat='pulse'):

    cmd = ['ffmpeg', '-n',
            '-f', aformat, '-i', adevice, '-acodec', acodec,
            '-f', vformat, '-i', vdevice, '-vcodec', vcodec,
            '-preset', preset, '-threads', str(threads),
            output_path]

    system.call(cmd, logger)

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
