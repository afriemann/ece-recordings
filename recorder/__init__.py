#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: TODO
   :platform: Unix
   :synopsis: TODO.

.. moduleauthor:: Aljosha Friemann aljosha.friemann@gmail.com

"""

VERSION = "0.0.7"

import os

from . import system, ffmpeg

def record(acodec, vcodec, threads, output, device_name = "VGA2USB"):
    if os.path.splitext(output)[1] == '':
        raise Exception("no format specified for output file: %s" % output)

    ffmpeg.record(adevice = system.get_audio_device("default"), acodec = acodec,
                  vdevice = system.get_video_device(device_name), vcodec = vcodec,
                  output_path = output)

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
