#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: TODO
   :platform: Unix
   :synopsis: TODO.

.. moduleauthor:: Aljosha Friemann aljosha.friemann@gmail.com

"""

import os, subprocess, logging, select

from . import utils

logger = logging.getLogger(__name__)

def read_file(path):
    content = None
    with open(path, 'r') as f:
        content = f.read()
    return content.strip()

def video_devices():
    device_root = '/sys/class/video4linux'

    if os.path.exists(device_root):
        for device in os.listdir(device_root):
            for root,dirs,files in os.walk(os.path.join(device_root, device)):
                yield (os.path.basename(root), read_file(os.path.join(root, 'name')))
                break
    else:
        raise Exception("no video4linux devices found in %s!" % device_root)

def get_video_device(name):
    if name == 'default':
        return name

    device_names = []
    for device, device_name in video_devices():
        if name in device_name:
            return "/dev/%s" % device
        else:
            device_names.append(device_name)

    raise Exception("could not find device %s %s" % (name, device_names))

def get_audio_device(name):
    if name == 'default':
        return name

    raise NotImplementedError('get_audio_device for name %s' % name)

def call(popenargs, logger, **kwargs):
    logger.debug('running `%s`', ' '.join(popenargs))

    child = subprocess.Popen(popenargs, **kwargs)
    child.communicate()

    return child.returncode

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
