#!/bin/sh

ffmpeg -n -f pulse -i default -acodec pcm_s16le \
          -f v4l2 -i /dev/video0 -vcodec libx264 \
          -preset ultrafast -threads 0
          output.mp4
