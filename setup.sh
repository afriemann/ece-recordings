#!/bin/sh -ex
# -*- coding: utf-8 -*-
# setup.sh
# created: 2015-11-02
# author: Aljosha Friemann aljosha.friemann@gmail.com

# directory safety. does not resolve links!
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

help () {
    echo -e "usage: $0 [COMMANDS]\n
    possible commands are:\n
    \t-h this help message\n"
}

log () {
    if which logger &>/dev/null; then
        # log to syslog daemon
        logger -s -t "$prog" "$@"
    else
        echo "$(date) ["$prog"] $@" >> "$LOGFILE"
    fi
}

while getopts "h" optname
    do
    case "$optname" in
        "h")
            help
            exit 0
            ;;
        "?")
            echo "unknown option $OPTARG"
            exit 1
            ;;
        ":")
            echo "no argument value for option $OPTARG"
            exit 1
            ;;
        *)
            # should not occur
            echo "unknown error while processing options"
            exit $?
            ;;
    esac
done && shift $((OPTIND-1))

# check for root
# [ "$EUID" -ne 0 ] && log "Please run as root!" && exit 1

wget "https://ssl.epiphan.com/downloads/linux/browse.php?dir=dists%2Fubuntu%2F14.04%2Fx86_64&file=vga2usb-3.30.1.40-ubuntu-3.16.0-46-generic-x86_64.deb" -O "vga2usb-3.30.1.40-ubuntu-3.16.0-46-generic-x86_64.deb"
sudo dpkg -i "vga2usb-3.30.1.40-ubuntu-3.16.0-46-generic-x86_64.deb" || sudo apt-get update
sudo apt-get install -f
sudo dpkg -i "vga2usb-3.30.1.40-ubuntu-3.16.0-46-generic-x86_64.deb" || :

if ! which ffmpeg; then
    sudo add-apt-repository ppa:mc3man/trusty-media
    sudo apt-get update
    sudo apt-get install ffmpeg python3-pip
fi

if ! grep -q "PATH.*~/.local/bin" /etc/environment; then
    sed 's/PATH="\(.*\)"/PATH="\1:~\/.local\/bin"/' /etc/environment
fi

pip3 install --user --egg --upgrade .

export PATH="$PATH:~/.local/bin"

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
